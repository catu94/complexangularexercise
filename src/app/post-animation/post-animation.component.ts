import { Component, OnInit } from '@angular/core';
import {
  animate,
  keyframes,
  query,
  stagger,
  style,
  transition,
  trigger
} from '@angular/animations';
import { Observable, from } from 'rxjs';
import { switchMap, concatMap, tap, filter, groupBy, mergeMap, toArray, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'post-animation',
  templateUrl: './post-animation.component.html',
  styleUrls: ['./post-animation.component.scss'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [

        query(':enter', style({ opacity: 0 }), { optional: true }),

        query(':enter', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({ opacity: 0, transform: 'translateY(-20px)', offset: 0 }),
            style({ opacity: .5, transform: 'translateY(35px)', offset: 0.4 }),
            style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
          ]))]), { optional: true }),

        query(':leave', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({ opacity: 1, transform: 'translateY(0)', offset: 0 }),
            style({ opacity: .5, transform: 'translateY(35px)', offset: 0.4 }),
            style({ opacity: 0, transform: 'translateY(-20px)', offset: 1.0 }),
          ]))]), { optional: true })
      ])
    ]),
  ],
})
export class PostAnimationComponent implements OnInit {

  comments: Comment[] = []

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  getData(): Observable<Object> {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/')
      .pipe(
        switchMap(post => from(post as any)),
        filter((post: any) => post.id % 2 == 0),
        concatMap((post: any) => this.http.get('https://jsonplaceholder.typicode.com/comments?postId=' + post.id)),  
        switchMap(comment => from(comment as any)),
        groupBy((comment: any) => comment.postId),
        mergeMap(group => group.pipe(toArray())),
        map(array => array[array.length - 1]),
        tap((comment: any) => console.log("Api Started " + comment.body))
      )
  }

  removeItems() {
    this.comments = [];
  }

  initItems() {
    var data$ = this.getData()
      .subscribe((x: any) => this.comments.push(new Comment(x.postId, x.id, x.name, x.email, x.body)));
  }
}

class Comment {
  constructor(public postId: number, public id: number, public name: string, public email: string, public body: string) { }
}
